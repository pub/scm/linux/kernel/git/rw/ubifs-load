CFLAGS:=-ansi -pedantic -Wall -Wextra -D_DEFAULT_SOURCE

fsstress: fsstress.c
	$(CC) $(CFLAGS) $^ -o $@

.PHONY: clean
clean:
	$(RM) fsstress *.o
